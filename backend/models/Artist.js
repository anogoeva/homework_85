const mongoose = require('mongoose');

const ArtistSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    unique: true,
    validate: {
      validator: async value => {
        const title = await Artist.findOne({title: value});
        if (title) return false;
      },
      message: 'Message error',
    },
  },
  image: String,
  information: String,
  published: {
    type: Boolean,
    default: false
  },
});

const Artist = mongoose.model('Artist', ArtistSchema);

module.exports = Artist;

