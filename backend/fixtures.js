const mongoose = require('mongoose');
const {nanoid} = require('nanoid');
const config = require('./config');
const Album = require("./models/Album");
const User = require("./models/User");
const Artist = require("./models/Artist");
const Track = require("./models/Track");

const run = async () => {
  await mongoose.connect(config.db.url);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }


  await User.create({
      username: 'admin',
      password: '123',
      token: nanoid(),
      role: 'admin',
      email: 'admin@admin.com',
      displayName: 'super admin',
    }, {
      username: 'root',
      password: '123',
      token: nanoid(),
      role: 'user',
      email: 'root@root.com',
      displayName: 'super root',
    }
  );

  const [MichaelJackson, Adele, Imany] = await Artist.create({
      title: 'Michael Jackson',
      image: 'fixtures/Mickael.jpeg',
      information: 'Родился 29 августа 1958 г.',
      published: true
    }, {
      title: 'Adele',
      image: 'fixtures/Adele.jpg',
      information: 'Родилась 5 августа 1988 г.',
      published: true
    },
    {
      title: 'Imany',
      image: 'fixtures/Imany.jpeg',
      information: 'Родилась 5 апреля 1979 г.',
      published: false
    });

  const [ThisIsIt, Adele_25, VoodooCello] = await Album.create({
      title: 'This is it',
      artist: MichaelJackson,
      year: '1979',
      image: 'fixtures/Mickael_album.jpg',
      published: true

    },
    {
      title: 'Adele_25',
      artist: Adele,
      year: '2021',
      image: 'fixtures/Adele_album.jpeg',
      published: true

    },
    {
      title: 'Voodoo Cello',
      artist: Imany,
      year: '2021',
      image: 'fixtures/Imany_album.jpeg',
      published: false

    });

  await Track.create({
      title: 'Remember the Time',
      album: ThisIsIt,
      number: 1,
      duration: '3',
      published: true
    },
    {
      title: 'Thriller',
      album: ThisIsIt,
      number: 2,
      duration: '4',
      published: true
    },
    {
      title: 'Bad',
      album: ThisIsIt,
      number: 3,
      duration: '3',
      published: true
    },
    {
      title: 'Beat it',
      album: ThisIsIt,
      number: 4,
      duration: '5',
      published: true
    },
    {
      title: 'Earth song',
      album: ThisIsIt,
      number: 5,
      duration: '3',
      published: false
    },
    {
      title: 'Rolling in the Deep',
      album: Adele_25,
      number: 1,
      duration: '4',
      published: true
    },
    {
      title: 'Someone like you',
      album: Adele_25,
      number: 2,
      duration: '3',
      published: true
    },
    {
      title: 'Hello',
      album: Adele_25,
      number: 3,
      duration: '5',
      published: true
    },
    {
      title: 'Set Fire To The Rain',
      album: Adele_25,
      number: 4,
      duration: '4',
      published: true
    },
    {
      title: 'Skyfall',
      album: Adele_25,
      number: 5,
      duration: '6',
      published: false
    },
    {
      title: 'Slow down',
      album: VoodooCello,
      number: 7,
      duration: '5',
      published: false
    },
    {
      title: 'Take care',
      album: VoodooCello,
      number: 2,
      duration: '3',
      published: false
    },
    {
      title: 'TGrey Monday',
      album: VoodooCello,
      number: 3,
      duration: '5',
      published: false
    },
    {
      title: 'Seat with me',
      album: VoodooCello,
      number: 4,
      duration: '4',
      published: false
    },
    {
      title: 'You will Never Know',
      album: VoodooCello,
      number: 5,
      duration: '5',
      published: false
    });

  await mongoose.connection.close();
};

run().catch(console.error);