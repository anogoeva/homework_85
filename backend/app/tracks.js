const express = require('express');
const Track = require('../models/Track');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const Artist = require("../models/Artist");
const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const query = {};
    if (req.query.album) {
      query.album = req.query.album;
    }
    const tracks = await Track.find(query).populate('album', 'title').sort({number: 1});
    res.send(tracks);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', auth, async (req, res) => {
  try {

    const trackData = {
      artist: req.body.artist,
      title: req.body.title,
      album: req.body.album,
      duration: req.body.duration || null,
      number: req.body.number || null
    };

    const track = new Track(trackData);

    await track.save();
    res.send(track);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.post('/:id/publish', auth, permit('admin'), async (req, res) => {
  try {
    const track = await Track.findById(req.params.id);

    if (Object.keys(track).length === 0) {
      return res.status(404).send({error: `Track ID=${req.params.id} is not found.`});
    } else {
      track.published = !track.published;
      await track.save({validateBeforeSave: false});
      return  res.send({message: `Track ${track.title} successfully published.`})
    }
  } catch (error) {
    res.status(404).send(error);
  }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const track = await Track.findByIdAndRemove(req.params.id);

    if (track) {
      res.send(`Track '${track.title} removed'`);
    } else {
      res.status(404).send({error: 'Track not found'});
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;