const express = require('express');
const Album = require('../models/Album');
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const User = require("../models/User");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
  try {
    const query = {};
    if (req.query.artist) {
      query.artist = req.query.artist;
    }
    const albums = await Album.find(query).populate('artist', 'title').sort({year: 1});
    res.send(albums);

  } catch (e) {
    console.log(e);
    res.sendStatus(500);
  }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
  try {
    const albumData = {
      title: req.body.title,
      artist: req.body.artist,
      year: req.body.year || null,
    };

    if (req.file) {
      albumData.image = 'uploads/' + req.file.filename;
    } else {
      albumData.image = null;
    }

    const album = new Album(albumData);

    await album.save();
    res.send(album);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.post('/:id/publish', auth, upload.single('image'), permit('admin'), async (req, res) => {
  try {
    const album = await Album.findById(req.params.id);

    if (Object.keys(album).length === 0) {
      return res.status(404).send({error: `Album ID=${req.params.id} is not found.`});
    } else {
      album.published = !album.published;
      await album.save({validateBeforeSave: false});
      return  res.send({message: `Album ${album.title} successfully published.`})
    }
  } catch (error) {
    res.status(404).send(error);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const album = await Album.findById(req.params.id).populate('artist', 'title');

    if (album) {
      res.send(album);
    } else {
      res.status(404).send({error: 'Album not found'});
    }
  } catch {
    res.sendStatus(500);
  }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const album = await Album.findByIdAndRemove(req.params.id);

    if (album) {
      res.send(`Album '${album.title} removed'`);
    } else {
      res.status(404).send({error: 'Album not found'});
    }
  } catch (e) {
    res.sendStatus(500);
  }
});


module.exports = router;