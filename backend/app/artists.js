const express = require('express');
const Artist = require('../models/Artist');
const path = require("path");
const multer = require('multer');
const config = require('../config');
const {nanoid} = require('nanoid');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const Album = require("../models/Album");
const User = require("../models/User");


const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res) => {

  try {
    const token = req.get('Authorization');
    const user = await User.findOne({token});
    let artists = null;
    if (token && user && user.role === 'admin') {
        artists = await Artist.find();
        res.send(artists);
    } else {
      artists = await Artist.find({published: true});
      res.send(artists);
    }
  } catch (e) {
    console.log(e);
    res.sendStatus(500);
  }
});

router.post('/', auth, upload.single('image'), async (req, res) => {
  try {
    const artistData = {
      title: req.body.title,
      information: req.body.information || null,
    };

  if (req.file) {
    artistData.image = 'uploads/' + req.file.filename;
  }

  const artist = new Artist(artistData);

    await artist.save();
    res.send(artist);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.post('/:id/publish', auth, upload.single('image'), permit('admin'), async (req, res) => {
  try {
    const artist = await Artist.findById(req.params.id);

    if (Object.keys(artist).length === 0) {
      return res.status(404).send({error: `Artist ID=${req.params.id} is not found.`});
    } else {
      artist.published = !artist.published;
      await artist.save({validateBeforeSave: false});
      return  res.send({message: `Artist ${artist.title} successfully published.`})
    }
  } catch (error) {
    res.status(404).send(error);
  }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const artist = await Artist.findByIdAndRemove(req.params.id);

    if (artist) {
      res.send(`Artist '${artist.title} removed'`);
    } else {
      res.status(404).send({error: 'Artist not found'});
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;