const express = require('express');
const dayjs = require("dayjs");
const TrackHistory = require("../models/TrackHistory");
const auth = require("../middleware/auth");
const router = express.Router();


router.post('/', auth, async (req, res) => {

  const trackHistoryData = {
    user: req.user._id,
    track: req.body.track,
    datetime: dayjs().format()
  };

  const trackHistory = new TrackHistory(trackHistoryData);
  try {
    await trackHistory.save();
    res.send(trackHistory);
  } catch (error) {
    res.status(500).send(error);
  }
});


router.get('/', auth, async (req, res) => {
  try {
    const trackHistoriesData = await TrackHistory.find({user: req.user._id}).populate('user', 'username').populate('track', 'title').sort({datetime: -1});
    res.send(trackHistoriesData);
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;