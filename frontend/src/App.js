import {Redirect, Route, Switch} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import Artists from "./containers/Artists/Artists";
import Tracks from "./containers/Tracks/Tracks";
import Albums from "./containers/Albums/Albums";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import {useSelector} from "react-redux";
import NewArtist from "./containers/NewArtist/NewArtist";
import NewAlbum from "./containers/NewAlbum/NewAlbum";
import NewTrack from "./containers/NewTrack/NewTrack";

const App = () => {
    const user = useSelector(state => state.users.user);

    const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
        return isAllowed ?
            <Route {...props}/> :
            <Redirect to={redirectTo}/>
    };
    return (
        <Layout>
            <Switch>
                <Route path="/" exact component={Artists}/>
                <ProtectedRoute
                    path="/artists/new"
                    component={NewArtist}
                    isAllowed={user}
                    redirectTo="/login"
                />
                <ProtectedRoute
                    path="/album/new"
                    component={NewAlbum}
                    isAllowed={user}
                    redirectTo="/login"
                />
                <ProtectedRoute
                    path="/track/new"
                    component={NewTrack}
                    isAllowed={user}
                    redirectTo="/login"
                />
                <Route path="/albums" component={Albums}/>
                <Route path="/tracks" component={Tracks}/>
                <Route path="/register" component={Register}/>
                <Route path="/login" component={Login}/>
                <Route path="/track_history" component={TrackHistory}/>
            </Switch>
        </Layout>
    );
};

export default App;
