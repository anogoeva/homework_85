import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from "redux-thunk";
import usersReducer, {initialState} from "./reducers/usersReducer";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";
import axiosApi from "../axiosApi";
import artistsReducer from "./reducers/artistsReducer";
import tracksReducer from "./reducers/tracksReducer";
import albumsReducer from "./reducers/albumsReducer";
import tracksHistoryReducer from "./reducers/tracksHistoryReducer";

const rootReducer = combineReducers({
    'artists': artistsReducer,
    'tracks': tracksReducer,
    'albums': albumsReducer,
    'users': usersReducer,
    'trackHistory': tracksHistoryReducer,

});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistedState = loadFromLocalStorage();

const store = createStore(
    rootReducer,
    persistedState,
    composeEnhancers(applyMiddleware(thunk))
);

store.subscribe(() => {
    saveToLocalStorage({
        users: {
            ...initialState,
            users: store.getState().users.user
        },
    });
});

axiosApi.interceptors.request.use(config => {
    try {
        config.headers['Authorization'] = store.getState().users.user.token
    } catch (e) {
    }
    return config;
});


axiosApi.interceptors.response.use(res => res, e => {
    if (!e.response) {
        e.response = {data: {global: 'No internet'}}
    }

    throw e;
});

export default store;