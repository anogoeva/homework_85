import axios from "axios";
import {apiURL} from "../../config";
import {toast} from "react-toastify";
import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";


export const FETCH_TRACKS_REQUEST = 'FETCH_TRACKS_REQUEST';
export const FETCH_TRACKS_SUCCESS = 'FETCH_TRACKS_SUCCESS';
export const FETCH_TRACKS_FAILURE = 'FETCH_TRACKS_FAILURE';


export const CREATE_TRACK_REQUEST = 'CREATE_TRACK_REQUEST';
export const CREATE_TRACK_SUCCESS = 'CREATE_TRACK_SUCCESS';
export const CREATE_TRACK_FAILURE = 'CREATE_TRACK_FAILURE';

export const DELETE_TRACK_REQUEST = 'DELETE_TRACK_REQUEST';
export const DELETE_TRACK_SUCCESS = 'DELETE_TRACK_SUCCESS';
export const DELETE_TRACK_FAILURE = 'DELETE_TRACK_FAILURE';


export const PUBLISH_TRACK_REQUEST = 'PUBLISH_TRACK_REQUEST';
export const PUBLISH_TRACK_SUCCESS = 'PUBLISH_TRACK_SUCCESS';
export const PUBLISH_TRACK_FAILURE = 'PUBLISH_TRACK_FAILURE';

export const deleteTrackRequest = () => ({type: DELETE_TRACK_REQUEST});
export const deleteTrackSuccess = (id) => ({type: DELETE_TRACK_SUCCESS, payload: id});
export const deleteTrackFailure = () => ({type: DELETE_TRACK_FAILURE});


export const fetchTracksRequest = () => ({type: FETCH_TRACKS_REQUEST});
export const fetchTracksSuccess = tracks => ({type: FETCH_TRACKS_SUCCESS, payload: tracks});
export const fetchTracksFailure = () => ({type: FETCH_TRACKS_FAILURE});


export const createTrackRequest = () => ({type: CREATE_TRACK_REQUEST});
export const createTrackSuccess = () => ({type: CREATE_TRACK_SUCCESS});
export const createTrackFailure = (error) => ({type: CREATE_TRACK_FAILURE, payload: error});


export const publishTrackRequest = (id) => ({type: PUBLISH_TRACK_REQUEST, payload: id});
export const publishTrackSuccess = () => ({type: PUBLISH_TRACK_SUCCESS});
export const publishTrackFailure = (error) => ({type: PUBLISH_TRACK_FAILURE, payload: error});

export const fetchTracks = id => {
    return async dispatch => {
        try {
            dispatch(fetchTracksRequest());
            const response = await axios.get(apiURL + '/tracks?album=' + id);
            dispatch(fetchTracksSuccess(response.data));
        } catch (e) {
            dispatch(fetchTracksFailure());
            if (e.response.status === 401) {
                toast.warning('You need login!');
            } else
                toast.error('Could not fetch artists!', {
                    theme: 'colored',
                });
        }
    };
};


export const createTrack = trackData => {
    return async dispatch => {
        try {
            dispatch(createTrackRequest());
            await axiosApi.post('/tracks', trackData);
            dispatch(createTrackSuccess());
            dispatch(historyPush('/'));
            toast.success('Track created');
        } catch (e) {
            dispatch(createTrackFailure(e.response.data));
            toast.error('Could not create track');
        }
    };
};


export const deleteTrack = id => {
    return async (dispatch) => {
        try {
            dispatch(deleteTrackRequest());
            await axiosApi.delete(`/tracks/${id}`);
            dispatch(deleteTrackSuccess());
            toast.success('Successfully deleted track with id - ' + id);
        } catch (error) {
            if (error.response.status === 403) {
                toast.error('You don\'t have permission for this operation');
            }
            dispatch(deleteTrackFailure(error));
        }
    };
};

export const publishTrack = id => {
    return async (dispatch) => {
        try {
            dispatch(publishTrackRequest());
            await axiosApi.post(`/tracks/${id}/publish`);
            dispatch(publishTrackSuccess());
            toast.success('Successfully publish track with id - ' + id);
        } catch (error) {
            if (error.response.status === 403) {
                toast.error('You don\'t have permission for this operation');
            }
            dispatch(publishTrackFailure(error));
        }
    };
};