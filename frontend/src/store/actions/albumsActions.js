import axios from "axios";
import {apiURL} from "../../config";
import {toast} from "react-toastify";
import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";


export const FETCH_ALBUMS_REQUEST = 'FETCH_ALBUMS_REQUEST';
export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
export const FETCH_ALBUMS_FAILURE = 'FETCH_ALBUMS_FAILURE';

export const CREATE_ALBUM_REQUEST = 'CREATE_ALBUM_REQUEST';
export const CREATE_ALBUM_SUCCESS = 'CREATE_ALBUM_SUCCESS';
export const CREATE_ALBUM_FAILURE = 'CREATE_ALBUM_FAILURE';

export const DELETE_ALBUM_REQUEST = 'DELETE_ALBUM_REQUEST';
export const DELETE_ALBUM_SUCCESS = 'DELETE_ALBUM_SUCCESS';
export const DELETE_ALBUM_FAILURE = 'DELETE_ALBUM_FAILURE';

// export const PUBLISH_ALBUM_REQUEST = 'PUBLISH_ALBUM_REQUEST';
// export const PUBLISH_ALBUM_SUCCESS = 'PUBLISH_ALBUM_SUCCESS';
// export const PUBLISH_ALBUM_FAILURE = 'PUBLISH_ALBUM_FAILURE';



export const fetchAlbumsRequest = () => ({type: FETCH_ALBUMS_REQUEST});
export const fetchAlbumsSuccess = albums => ({type: FETCH_ALBUMS_SUCCESS, payload: albums});
export const fetchAlbumsFailure = () => ({type: FETCH_ALBUMS_FAILURE});

export const createAlbumRequest = () => ({type: CREATE_ALBUM_REQUEST});
export const createAlbumSuccess = () => ({type: CREATE_ALBUM_SUCCESS});
export const createAlbumFailure = (error) => ({type: CREATE_ALBUM_FAILURE, payload: error});


export const deleteAlbumRequest = () => ({type: DELETE_ALBUM_REQUEST});
export const deleteAlbumSuccess = () => ({type: DELETE_ALBUM_SUCCESS});
export const deleteAlbumFailure = () => ({type: DELETE_ALBUM_FAILURE});

// export const publishAlbumRequest = () => ({type: PUBLISH_ALBUM_REQUEST});
// export const publishAlbumSuccess = () => ({type: PUBLISH_ALBUM_SUCCESS});
// export const publishAlbumFailure = () => ({type: PUBLISH_ALBUM_FAILURE});



export const fetchAlbums = id => {
    return async dispatch => {
        try {
            dispatch(fetchAlbumsRequest());
            const response = await axios.get(apiURL + '/albums?artist=' + id);
            dispatch(fetchAlbumsSuccess(response.data));
        } catch (e) {
            dispatch(fetchAlbumsFailure());
            if (e.response.status === 401) {
                toast.warning('You need login!');
            } else
                toast.error('Could not fetch artists!', {
                    theme: 'colored',
                });
        }
    };
};

export const createAlbum = albumData => {
    return async dispatch => {
        try {
            dispatch(createAlbumRequest());
            await axiosApi.post('/albums', albumData);
            dispatch(createAlbumSuccess());
            dispatch(historyPush('/'));
            toast.success('Album created');
        } catch (e) {
            dispatch(createAlbumFailure(e.response.data));
            toast.error('Could not create album');
            }
    };
};

export const deleteAlbum = id => {
    return async (dispatch) => {
        try {
            dispatch(deleteAlbumRequest());
            await axiosApi.delete(`/albums/${id}`);
            dispatch(deleteAlbumSuccess());
            toast.success('Successfully deleted album with id - ' + id);
        } catch (error) {
            if (error.response.status === 403) {
                toast.error('You don\'t have permission for this operation');
            }
            dispatch(deleteAlbumFailure(error));
        }
    };
};