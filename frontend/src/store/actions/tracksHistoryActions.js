import axios from "axios";
import {apiURL} from "../../config";
import {toast} from "react-toastify";

export const ADD_TRACK_HISTORY_REQUEST = 'ADD_TRACK_HISTORY_REQUEST';
export const ADD_TRACK_HISTORY_SUCCESS = 'ADD_TRACK_HISTORY_SUCCESS';
export const ADD_TRACK_HISTORY_FAILURE = 'ADD_TRACK_HISTORY_FAILURE';


export const addTrackHistoryRequest = () => ({type: ADD_TRACK_HISTORY_REQUEST});
export const addTrackHistorySuccess = () => ({type: ADD_TRACK_HISTORY_SUCCESS});
export const addTrackHistoryFailure = () => ({type: ADD_TRACK_HISTORY_FAILURE});


export const FETCH_TRACK_HISTORY_REQUEST = 'FETCH_TRACK_HISTORY_REQUEST';
export const FETCH_TRACK_HISTORY_SUCCESS = 'FETCH_TRACK_HISTORY_SUCCESS';
export const FETCH_TRACK_HISTORY_FAILURE = 'FETCH_TRACK_HISTORY_FAILURE';


export const fetchTrackHistoryRequest = () => ({type: FETCH_TRACK_HISTORY_REQUEST});
export const fetchTrackHistorySuccess = (trackHistory) => ({type: FETCH_TRACK_HISTORY_SUCCESS, payload: trackHistory});
export const fetchTrackHistoryFailure = () => ({type: FETCH_TRACK_HISTORY_FAILURE});


export const addTrackToTrackHistory = (track, trackTitle) => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization': getState().users.user && getState().users.user.token
            };
            dispatch(addTrackHistoryRequest());
            const response = await axios.post(apiURL + '/track_history', track, {headers});
            if (response.data) {
                dispatch(addTrackHistorySuccess());
                toast.success('Track ' + trackTitle + ' successfully added to track history')
            }
        } catch (e) {
            dispatch(addTrackHistoryFailure());
            toast.error('Something went wrong', {
                theme: 'colored',
            });
        }
    };
};

export const fetchTrackHistory = () => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization': getState().users.user && getState().users.user.token
            };
            dispatch(fetchTrackHistoryRequest());
            const response = await axios.get(apiURL + '/track_history', {headers});
            if (response.data) {
                dispatch(fetchTrackHistorySuccess(response.data));
            }
        } catch (e) {
            dispatch(fetchTrackHistoryFailure());
            if (e.response.status === 401) {
                toast.warning('You need login!');
            } else
                toast.error('Could not fetch artists!', {
                    theme: 'colored',
                });
        }
    };
};