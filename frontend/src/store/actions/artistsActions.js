import {toast} from "react-toastify";
import axiosApi from "../../axiosApi";
import {historyPush} from "./historyActions";


export const FETCH_ARTISTS_REQUEST = 'FETCH_ARTISTS_REQUEST';
export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const FETCH_ARTISTS_FAILURE = 'FETCH_ARTISTS_FAILURE';


export const CREATE_ARTIST_REQUEST = 'CREATE_ARTIST_REQUEST';
export const CREATE_ARTIST_SUCCESS = 'CREATE_ARTIST_SUCCESS';
export const CREATE_ARTIST_FAILURE = 'CREATE_ARTIST_FAILURE';


export const DELETE_ARTIST_REQUEST = 'DELETE_ARTIST_REQUEST';
export const DELETE_ARTIST_SUCCESS = 'DELETE_ARTIST_SUCCESS';
export const DELETE_ARTIST_FAILURE = 'DELETE_ARTIST_FAILURE';


export const PUBLISH_ARTIST_REQUEST = 'PUBLISH_ARTIST_REQUEST';
export const PUBLISH_ARTIST_SUCCESS = 'PUBLISH_ARTIST_SUCCESS';
export const PUBLISH_ARTIST_FAILURE = 'PUBLISH_ARTIST_FAILURE';


export const ADD_ARTIST_NAME = 'ADD_ARTIST_NAME'

export const fetchArtistsRequest = () => ({type: FETCH_ARTISTS_REQUEST});
export const fetchArtistsSuccess = artists => ({type: FETCH_ARTISTS_SUCCESS, payload: artists});
export const fetchArtistsFailure = () => ({type: FETCH_ARTISTS_FAILURE});

export const createArtistRequest = () => ({type: CREATE_ARTIST_REQUEST});
export const createArtistSuccess = () => ({type: CREATE_ARTIST_SUCCESS});
export const createArtistFailure = (error) => ({type: CREATE_ARTIST_FAILURE, payload: error});


export const deleteArtistRequest = () => ({type: DELETE_ARTIST_REQUEST});
export const deleteArtistSuccess = (id) => ({type: DELETE_ARTIST_SUCCESS, payload: id});
export const deleteArtistFailure = () => ({type: DELETE_ARTIST_FAILURE});


export const publishArtistRequest = () => ({type: PUBLISH_ARTIST_REQUEST});
export const publishArtistSuccess = (id) => ({type: PUBLISH_ARTIST_SUCCESS, payload: id});
export const publishArtistFailure = (error) => ({type: PUBLISH_ARTIST_FAILURE, payload: error});

export const addArtistName = artistName => ({type: ADD_ARTIST_NAME, payload: artistName});

export const fetchArtists = () => {
    return async (dispatch, getState) => {
        try {
            const headers = {
                'Authorization': getState().users.user && getState().users.user.token
            };
            dispatch(fetchArtistsRequest());
            const response = await axiosApi.get('/artists', {headers});
            dispatch(fetchArtistsSuccess(response.data));
        } catch (error) {
            dispatch(fetchArtistsFailure());
            if (error.message === 'Network Error') {
                toast.warning('Network Error!');
            } else
                toast.error('Could not fetch artists!', {
                    theme: 'colored',
                });
        }
    };
};


export const createArtist = artistData => {
    return async dispatch => {
        try {
            dispatch(createArtistRequest());
            await axiosApi.post('/artists', artistData);
            dispatch(createArtistSuccess());
            dispatch(historyPush('/'));
            toast.success('Artist created');
        } catch (e) {
            dispatch(createArtistFailure(e.response.data));
            toast.error('Could not create artist');
        }
    };
};

export const publishArtist = id => {
    return async (dispatch) => {
        try {
            dispatch(publishArtistRequest());
            await axiosApi.post(`/artists/${id}/publish`);
            dispatch(publishArtistSuccess(id));
            toast.success('Successfully published artist with id - ' + id);
        } catch (error) {
            if (error.response.status === 403) {
                toast.error('You don\'t have permission for this operation');
            }
            dispatch(publishArtistFailure(error));
        }
    };
};

export const deleteArtist = id => {
    return async (dispatch) => {
        try {
            dispatch(deleteArtistRequest());
            await axiosApi.delete(`/artists/${id}`);
            dispatch(deleteArtistSuccess(id));
            toast.success('Successfully deleted artist with id - ' + id);
        } catch (error) {
            if (error.response.status === 403) {
                toast.error('You don\'t have permission for this operation');
            }
            dispatch(deleteArtistFailure(error));
        }
    };
};