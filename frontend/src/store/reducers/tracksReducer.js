import {
    CREATE_TRACK_FAILURE,
    CREATE_TRACK_REQUEST,
    CREATE_TRACK_SUCCESS,
    DELETE_TRACK_FAILURE,
    DELETE_TRACK_REQUEST,
    DELETE_TRACK_SUCCESS,
    FETCH_TRACKS_FAILURE,
    FETCH_TRACKS_REQUEST,
    FETCH_TRACKS_SUCCESS, PUBLISH_TRACK_FAILURE, PUBLISH_TRACK_REQUEST, PUBLISH_TRACK_SUCCESS,
} from "../actions/tracksActions";


const initialState = {
    fetchLoading: false,
    singleLoading: false,
    trackLoading: false,
    trackError: false,
    tracks: [],
    track: null,
};

const tracksReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRACKS_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_TRACKS_SUCCESS:
            return {...state, fetchLoading: false, tracks: action.payload};
        case FETCH_TRACKS_FAILURE:
            return {...state, fetchLoading: false};
        case CREATE_TRACK_REQUEST:
            return {...state, trackLoading: true};
        case CREATE_TRACK_SUCCESS:
            return {...state, trackLoading: false, trackError: null};
        case CREATE_TRACK_FAILURE:
            return {...state, trackLoading: false, trackError: action.payload};
        case DELETE_TRACK_REQUEST:
            return {...state, singleLoading: true};
        case DELETE_TRACK_SUCCESS:
            return {...state, singleLoading: false};
        case DELETE_TRACK_FAILURE:
            return {...state, singleLoading: false};
        case PUBLISH_TRACK_REQUEST:
            return {...state, trackLoading: true};
        case PUBLISH_TRACK_SUCCESS:
            return {...state, trackLoading: false, trackError: null};
        case PUBLISH_TRACK_FAILURE:
            return {...state, trackLoading: false, trackError: action.payload};
        default:
            return state;
    }
};

export default tracksReducer;