import {
    CREATE_ALBUM_FAILURE,
    CREATE_ALBUM_REQUEST,
    CREATE_ALBUM_SUCCESS,
    DELETE_ALBUM_FAILURE,
    DELETE_ALBUM_REQUEST,
    DELETE_ALBUM_SUCCESS,
    FETCH_ALBUMS_FAILURE,
    FETCH_ALBUMS_REQUEST,
    FETCH_ALBUMS_SUCCESS
} from "../actions/albumsActions";


const initialState = {
    albumsLoading: false,
    albumLoading: false,
    albums: [],
    album: null,
    albumError: null,
    singleLoading: false,

};

const albumsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALBUMS_REQUEST:
            return {...state, albumsLoading: true};
        case FETCH_ALBUMS_SUCCESS:
            return {...state, albumsLoading: false, albums: action.payload};
        case FETCH_ALBUMS_FAILURE:
            return {...state, albumsLoading: false};
        case CREATE_ALBUM_REQUEST:
            return {...state, albumLoading: true};
        case CREATE_ALBUM_SUCCESS:
            return {...state, albumLoading: false, albumError: null};
        case CREATE_ALBUM_FAILURE:
            return {...state, albumLoading: false, albumError: action.payload};
        case DELETE_ALBUM_REQUEST:
            return {...state, singleLoading: true};
        case DELETE_ALBUM_SUCCESS:
            return {...state, singleLoading: false};
        case DELETE_ALBUM_FAILURE:
            return {...state, singleLoading: false};
        // case PUBLISH_ALBUM_REQUEST:
        //     return {...state, albumLoading: true};
        // case PUBLISH_ALBUM_SUCCESS:
        //     return {...state, albumLoading: false, albumError: null};
        // case PUBLISH_ALBUM_FAILURE:
        //     return {...state, albumLoading: false, albumError: action.payload};
        default:
            return state;
    }
};

export default albumsReducer;