import {
    ADD_ARTIST_NAME,
    CREATE_ARTIST_FAILURE,
    CREATE_ARTIST_REQUEST,
    CREATE_ARTIST_SUCCESS,
    DELETE_ARTIST_FAILURE,
    DELETE_ARTIST_REQUEST,
    DELETE_ARTIST_SUCCESS,
    FETCH_ARTISTS_FAILURE,
    FETCH_ARTISTS_REQUEST,
    FETCH_ARTISTS_SUCCESS, PUBLISH_ARTIST_FAILURE, PUBLISH_ARTIST_REQUEST, PUBLISH_ARTIST_SUCCESS
} from "../actions/artistsActions";

const initialState = {
    fetchLoading: false,
    singleLoading: false,
    artistLoading: false,
    artists: [],
    artistName: null,
    artist: null,
    artistError: null
};

const artistsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ARTISTS_REQUEST:
            return {...state, fetchLoading: true};
        case FETCH_ARTISTS_SUCCESS:
            return {...state, fetchLoading: false, artists: action.payload};
        case FETCH_ARTISTS_FAILURE:
            return {...state, fetchLoading: false};
        case CREATE_ARTIST_REQUEST:
            return {...state, artistLoading: true};
        case CREATE_ARTIST_SUCCESS:
            return {...state, artistLoading: false, artistError: null};
        case CREATE_ARTIST_FAILURE:
            return {...state, artistLoading: false, artistError: action.payload};
        case ADD_ARTIST_NAME:
            return {...state, fetchLoading: false, artistName: action.payload};
        case DELETE_ARTIST_REQUEST:
            return {...state, singleLoading: true};
        case DELETE_ARTIST_SUCCESS:
            const newState = state.artists.filter(val => val._id !== action.payload);
            return {...state, singleLoading: false, artists: newState};
        case DELETE_ARTIST_FAILURE:
            return {...state, singleLoading: false};
        case PUBLISH_ARTIST_REQUEST:
            return {...state, artistLoading: true};
        case PUBLISH_ARTIST_SUCCESS:
            const publishState = state.artists.filter(val => val._id === action.payload);
            publishState[0].published = true;
            const returnState = state.artists.filter(val => val._id !== action.payload);
            returnState.push(publishState[0]);
            return {...state, artistLoading: false, artistError: null, artists: returnState};
        case PUBLISH_ARTIST_FAILURE:
            return {...state, artistLoading: false, artistError: action.payload};
        default:
            return state;
    }
};

export default artistsReducer;