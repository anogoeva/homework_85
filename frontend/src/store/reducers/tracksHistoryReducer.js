import {
    ADD_TRACK_HISTORY_FAILURE,
    ADD_TRACK_HISTORY_REQUEST,
    ADD_TRACK_HISTORY_SUCCESS,
    FETCH_TRACK_HISTORY_FAILURE,
    FETCH_TRACK_HISTORY_REQUEST,
    FETCH_TRACK_HISTORY_SUCCESS
} from "../actions/tracksHistoryActions";

const initialState = {
    tracksHistoryLoading: false,
    tracksHistory: null
};

const tracksReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_TRACK_HISTORY_REQUEST:
            return {...state, tracksHistoryLoading: true};
        case ADD_TRACK_HISTORY_SUCCESS:
            return {...state, tracksHistoryLoading: false};
        case ADD_TRACK_HISTORY_FAILURE:
            return {...state, tracksHistoryLoading: false};
        case FETCH_TRACK_HISTORY_REQUEST:
            return {...state, tracksHistoryLoading: true};
        case FETCH_TRACK_HISTORY_SUCCESS:
            return {...state, tracksHistoryLoading: false, tracksHistory: action.payload};
        case FETCH_TRACK_HISTORY_FAILURE:
            return {...state, tracksHistoryLoading: false};
        default:
            return state;
    }
};

export default tracksReducer;