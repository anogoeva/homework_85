import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {addArtistName} from "../../store/actions/artistsActions";
import {Box, Button, CircularProgress, Grid, makeStyles, Paper, Typography} from "@material-ui/core";
import {apiURL} from "../../config";
import imageNotAvailable from "../../assets/images/not_available.png";
import {Link} from "react-router-dom";
import {deleteAlbum, fetchAlbums} from "../../store/actions/albumsActions";


const useStyles = makeStyles(theme => ({
    link: {
        textDecoration: 'none',
        textTransform: "uppercase",
        color: 'grey',
        borderBottom: '2px solid black',
        fontSize: '40px'
    }
}));

const Albums = ({history}) => {
    const user = useSelector(state => state.users.user);
    const classes = useStyles();
    const dispatch = useDispatch();
    const albums = useSelector(state => state.albums.albums);
    const artists = useSelector(state => state.artists.artists);
    const albumsLoading = useSelector(state => state.albums.albumsLoading);

    const deleteHandle = (albumId) => {
        dispatch(deleteAlbum(albumId));
    };

    const artistId = new URLSearchParams(history.location.search).get("artist");
    let artistName = null;

    artists.map(artist => {
        if (artist._id === artistId) {
            dispatch(addArtistName(artist.title));
            artistName = artist.title;
        }
        return artistName;
    });

    useEffect(() => {
        dispatch(fetchAlbums(artistId));
    }, [dispatch, artistId]);

    return albums && (
        <Paper component={Box} p={2}>
            <Typography variant="h3">{artistName}</Typography>

            {albumsLoading ? (
                <Grid container justifyContent="center" alignItems="center">
                    <Grid item>
                        <CircularProgress/>
                    </Grid>
                </Grid>
            ) : albums.length === 0 ? <Typography variant="h6">"Albums not found"</Typography> :
                albums.map(album =>
                    <div key={album._id}>
                        <Link className={classes.link} to={'/tracks?album=' + album._id}>{album.title}</Link>
                        <Typography variant="h6">{album.year}</Typography>
                        {album.image ?
                            <img src={apiURL + '/' + album.image} alt='imagePhoto' width="100" height="100"/> :
                            <img src={imageNotAvailable} width="200" height="100" alt="Not Available"/>}
                        <div>{user && user.role === "admin" ? (
                            <Button variant="outlined" color="secondary" aria-controls="simple-menu"
                                    aria-haspopup="true"
                                    onClick={() => deleteHandle(album._id)}
                            >Delete</Button>
                        ) : ""}
                        </div>
                        <hr/>

                    </div>
                )}
        </Paper>
    );
};

export default Albums;