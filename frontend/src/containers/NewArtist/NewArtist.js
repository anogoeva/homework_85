import React from 'react';
import {Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {createArtist} from "../../store/actions/artistsActions";
import ArtistForm from "../../components/ArtistForm/ArtistForm";


const NewArtist = () => {
    const dispatch = useDispatch();
    const error = useSelector(state => state.artists.artistError);
    const loading = useSelector(state => state.artists.artistLoading);


    const onSubmit = artistData => {
        dispatch(createArtist(artistData));

    };

    return (
        <>
            <Typography variant="h4">New artist</Typography>
            <ArtistForm
                onSubmit={onSubmit}
                error={error}
                loading={loading}
            />
        </>
    );
};

export default NewArtist;