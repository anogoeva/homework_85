import React from 'react';
import {Typography} from "@material-ui/core";
import TrackForm from "../../components/TrackForm/TrackForm";
import {useDispatch, useSelector} from "react-redux";
import {createTrack} from "../../store/actions/tracksActions";

const NewTrack = () => {
    const dispatch = useDispatch();
    const artists = useSelector(state => state.artists.artists);
    const albums = useSelector(state => state.albums.albums);
    const error = useSelector(state => state.tracks.trackError);
    const loading = useSelector(state => state.tracks.trackLoading);

    const onSubmit = async trackData => {
        await dispatch(createTrack(trackData));
    };

    return (
        <>
            <Typography variant="h4">New track</Typography>
            <TrackForm
                onSubmit={onSubmit}
                artists={artists}
                albums={albums}
                error={error}
                loading={loading}
            />
        </>
    );
};

export default NewTrack;