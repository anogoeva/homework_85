import React, {useEffect} from 'react';
import {Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {createAlbum} from "../../store/actions/albumsActions";
import {fetchArtists} from "../../store/actions/artistsActions";
import AlbumForm from "../../components/AlbumForm/AlbumForm";

const NewAlbum = () => {

    const dispatch = useDispatch();
    const artists = useSelector(state => state.artists.artists);
    const loading = useSelector(state => state.albums.albumLoading);
    const error = useSelector(state => state.albums.albumError);

    useEffect(() => {
        dispatch(fetchArtists());
    }, [dispatch]);

    const onSubmit = async albumData => {
        await dispatch(createAlbum(albumData));
    };

    return (
        <>
            <Typography variant="h4">New album</Typography>
            <AlbumForm
                onSubmit={onSubmit}
                artists={artists}
                error={error}
                loading={loading}
            />
        </>
    );
};

export default NewAlbum;