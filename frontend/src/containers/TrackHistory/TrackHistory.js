import React, {useEffect} from 'react';
import {fetchTrackHistory} from "../../store/actions/tracksHistoryActions";
import {useDispatch, useSelector} from "react-redux";
import {Box, CircularProgress, Grid, Paper, Typography} from "@material-ui/core";
import {Redirect} from "react-router-dom";

const TrackHistory = () => {
    const dispatch = useDispatch();
    const tracksHistoryLoading = useSelector(state => state.trackHistory.tracksHistoryLoading);
    const trackHistory = useSelector(state => state.trackHistory.tracksHistory);
    const user = useSelector(state => state.users.user);

    useEffect(() => {
        dispatch(fetchTrackHistory());
    }, [dispatch]);

    if (!user) {
        return <Redirect to="/login"/>
    }
    return (
        <Paper component={Box} p={2}>
            {tracksHistoryLoading ? (
                <Grid container justifyContent="center" alignItems="center">
                    <Grid item>
                        <CircularProgress/>
                    </Grid>
                </Grid>
            ) : trackHistory === null ? <Typography variant="h6">"Empty track history"</Typography> :
                trackHistory.map(track =>
                    <div key={track._id}>
                        <Typography variant="h6">{track.track.title}</Typography>
                        <Typography variant="h6">{track.datetime}</Typography>
                        <hr/>
                    </div>
                )}
        </Paper>
    );
};

export default TrackHistory;