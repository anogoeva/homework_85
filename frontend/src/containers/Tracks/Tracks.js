import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchTracks, publishTrack} from "../../store/actions/tracksActions";
import {Box, Button, CircularProgress, Grid, Paper, Typography} from "@material-ui/core";
import {addTrackToTrackHistory} from "../../store/actions/tracksHistoryActions";
import {deleteTrack} from "../../store/actions/tracksActions";


const Tracks = ({history}) => {
    const user = useSelector(state => state.users.user);
    const dispatch = useDispatch();
    const tracks = useSelector(state => state.tracks.tracks);
    const artistName = useSelector(state => state.artists.artistName);
    const tracksLoading = useSelector(state => state.tracks.tracksLoading);

    const albumId = new URLSearchParams(history.location.search).get("album");

    const submitAddTrackToHistoryHandler = (trackId, trackTitle) => {
        dispatch(addTrackToTrackHistory({track: trackId}, trackTitle));

    };
    const deleteHandle = (trackId) => {
        dispatch(deleteTrack(trackId));
    };

    const publishHandle = (trackId) => {;
        dispatch(publishTrack(trackId));
    };

    useEffect(() => {
        dispatch(fetchTracks(albumId));
    }, [dispatch, albumId]);

    return tracks && (
        <Paper component={Box} p={2}>
            <Typography variant="h3">{artistName}</Typography>
            <Typography variant="h4"><b>{tracks[0] ? tracks[0].album.title : "This album is empty"}</b></Typography>

            {tracksLoading ? (
                <Grid container justifyContent="center" alignItems="center">
                    <Grid item>
                        <CircularProgress/>
                    </Grid>
                </Grid>
            ) : tracks.map(track =>
                <div key={track._id}>
                    <Typography variant="h6"
                                onClick={() => submitAddTrackToHistoryHandler(track._id, track.title)}><b>{track.title}</b></Typography>
                    <Typography variant="h6">Duration: <i><b>{track.duration}</b> min</i></Typography>
                    <Typography variant="h6">Track number: <i><b>{track.number}</b></i></Typography>
                    <Typography variant="h6">Track published: <i><b>{track.published === false ? 'Not published' : 'Published'}</b></i></Typography>
                    {user && user.role === 'admin' ? (
                        <Button variant="outlined" color="secondary" aria-controls="simple-menu" aria-haspopup="true"
                                onClick={() => deleteHandle(track._id)}
                        >Delete</Button>
                    ) : ""}
                    {track.published === false ? (
                        <Button variant="outlined" color="primary" aria-controls="simple-menu" aria-haspopup="true"
                                onClick={() => publishHandle(track._id)}
                        >Publish</Button>
                    ) : ""}
                    <hr/>
                </div>
            )}
        </Paper>
    );
};

export default Tracks;