import React, {useEffect, useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import FormElement from "../UI/Form/FormElement";
import {fetchAlbums} from "../../store/actions/albumsActions";
import {useDispatch, useSelector} from "react-redux";
import {fetchArtists} from "../../store/actions/artistsActions";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const TrackForm = ({onSubmit, error, loading}) => {
    const albums = useSelector(state => state.albums.albums);
    const artists = useSelector(state => state.artists.artists);
    const dispatch = useDispatch();
    const classes = useStyles();

    const [state, setState] = useState({
        title: "",
        artist: "",
        album: "",
        number: "",
        duration: "",
    });

    useEffect(() => {
        dispatch(fetchArtists());
        dispatch(fetchAlbums(state.artist));
    }, [dispatch, state.artist]);

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = {};
        formData.title = state.title;
        formData.artist = state.artist;
        formData.album = state.album;
        formData.number = state.number;
        formData.duration = state.duration;
        onSubmit(formData, error);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    return (
        <Grid
            container
            direction="column"
            spacing={2}
            component="form"
            className={classes.root}
            autoComplete="off"
            onSubmit={submitFormHandler}
            noValidate
        >
            <FormElement
                select
                options={artists ? artists : []}
                label="Artists"
                name="artist"
                value={state.artist}
                onChange={inputChangeHandler}
                error={getFieldError('artist')}
                required
            />
            <FormElement
                select
                options={albums ? albums : []}
                label="Album"
                name="album"
                value={state.album}
                onChange={inputChangeHandler}
                error={getFieldError('album')}
                required
            />

            <FormElement
                label="Title"
                name="title"
                value={state.title}
                onChange={inputChangeHandler}
                error={getFieldError('title')}
                required
            />

            <FormElement
                type="number"
                label="Number"
                name="number"
                value={state.number}
                onChange={inputChangeHandler}
            />

            <FormElement
                label="Duration"
                name="duration"
                value={state.duration}
                onChange={inputChangeHandler}
            />

            <Grid item xs={12}>
                <ButtonWithProgress
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    loading={loading}
                    disabled={loading}
                >
                    Create
                </ButtonWithProgress>
            </Grid>
        </Grid>
    );
};

export default TrackForm;