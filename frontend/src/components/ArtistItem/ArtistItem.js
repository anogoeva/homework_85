import {Button, Card, CardActions, CardHeader, CardMedia, Grid, IconButton, makeStyles} from "@material-ui/core";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import PropTypes from 'prop-types';
import {Link, useHistory} from "react-router-dom";
import imageNotAvailable from '../../assets/images/not_available.png';
import {apiURL} from "../../config";
import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {deleteArtist, publishArtist} from "../../store/actions/artistsActions";

const useStyles = makeStyles({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    }
});

const ArtistItem = ({title, image, id, published}) => {
    const dispatch = useDispatch();
    const history = useHistory();
    const user = useSelector(state => state.users.user);
    const classes = useStyles();
    let cardImage = imageNotAvailable;

    const deleteHandle = (artistId) => {
        dispatch(deleteArtist(artistId));
        history.push('/');
    };

    const publishHandle = (artistId) => {
        dispatch(publishArtist(artistId));
        history.push('/');
    };

    if (image) {
        cardImage = apiURL + '/' + image;
    }

    return (
        <Grid item xs={12} sm={6} md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader title={title}/>
                <CardMedia
                    image={cardImage}
                    title={title}
                    className={classes.media}
                />
                <CardActions>
                    <IconButton component={Link} to={'/albums?artist=' + id}>
                        <ArrowForwardIcon/>
                    </IconButton>
                </CardActions>
                { user && user.role === 'admin' ? (
                    <Button variant="outlined" color="secondary" aria-controls="simple-menu" aria-haspopup="true"
                            onClick={() => deleteHandle(id)}
                    >Delete</Button>
                ) : ""}
                { user && user.role === 'admin' ? (
                    <Button variant="outlined" color="primary" aria-controls="simple-menu" aria-haspopup="true" disabled={!!published}
                            onClick={() => publishHandle(id)}
                    >{published ? 'Published' : 'Publish'}</Button>
                ) : ""}
            </Card>

        </Grid>
    );
};

ArtistItem.propTypes = {
    title: PropTypes.string.isRequired,
    image: PropTypes.string,
    id: PropTypes.string.isRequired,
};

export default ArtistItem;